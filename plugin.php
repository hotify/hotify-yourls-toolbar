<?php
/*
Plugin Name: Hotify Toolbar
Plugin URI: http://yourls.org/
Description: Add a toolbar to redirected short URLs. Forked from yourls-toolbar.
Version: 1.0
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

global $hfy_toolbar;
$hfy_toolbar['do'] = false;
$hfy_toolbar['keyword'] = '';

// When a redirection to a shorturl is about to happen, register variables
yourls_add_action( 'redirect_shorturl', 'hfy_toolbar_add' );
function hfy_toolbar_add( $args ) {
  global $hfy_toolbar;
  $hfy_toolbar['do'] = true;
  $hfy_toolbar['keyword'] = $args[1];
}

// On redirection, check if this is a toolbar and draw it if needed
yourls_add_action( 'pre_redirect', 'hfy_toolbar_do' );
function hfy_toolbar_do( $args ) {
  global $hfy_toolbar;
  
  // Does this redirection need a toolbar?
  if( !$hfy_toolbar['do'] )
    return;

  // Do we have a cookie stating the user doesn't want a toolbar?
  if( isset( $_COOKIE['yourls_no_toolbar'] ) && $_COOKIE['yourls_no_toolbar'] == 1 )
    return;
  
  // Get URL and page title
  $url = $args[0];
  $pagetitle = yourls_get_keyword_title( $hfy_toolbar['keyword'] );

  // Update title if it hasn't been stored yet
  if( $pagetitle == '' ) {
    $pagetitle = yourls_get_remote_title( $url );
    yourls_edit_link_title( $hfy_toolbar['keyword'], $pagetitle );
  }
  $_pagetitle = htmlentities( yourls_get_remote_title( $url ) );
  
  $www = YOURLS_SITE;
  $ver = YOURLS_VERSION;
  $md5 = md5( $url );
  $sql = yourls_get_num_queries();

  // When was the link created (in days)
  $diff = abs( time() - strtotime( yourls_get_keyword_timestamp( $hfy_toolbar['keyword'] ) ) );
  $days = floor( $diff / (60*60*24) );
  if( $days == 0 ) {
    $created = 'today';
  } else {
    $created = $days . ' ' . yourls_n( 'day', 'days', $days ) . ' ago';
  }
  
  // How many hits on the page
  $hits = 1 + yourls_get_keyword_clicks( $hfy_toolbar['keyword'] );
  $hits = $hits . ' ' . yourls_n( 'view', 'views', $hits );
  
  // Plugin URL (no URL is hardcoded)
  $pluginurl = YOURLS_PLUGINURL . '/'.yourls_plugin_basename( dirname(__FILE__) );

  // All set. Draw the toolbar itself.
  echo <<<PAGE
<!DOCTYPE html>
<html>
<head>
  <title>$_pagetitle</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="icon" type="image/gif" href="/images/favicon.gif" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0">  
  <meta http-equiv="X-UA-Compatible" content="IE-9"/>
  <meta name="ROBOTS" content="NOINDEX, FOLLOW" />
  <style type="text/css">
    .clearfix {
      width: 100%;
      height: 100%;
    }
    .clearfix:after {
       content: " ";
       visibility: hidden;
       display: block;
       height: 0;
       clear: both;
    }
    html, body, #container, #frame {
      padding: 0;
      margin: 0;
      overflow: hidden;
      width: 100%;
      height: 100%;
      font-size: 100%;
      background: white;
    }
    #container {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
      background: #E53935;
    }
    #about {
      font-family: "Helvetica Neue", "Helvetica", sans-serif;
      color: #fff;
      padding: 5px 8px 5px 25px;
      position: relative;
      max-width: 600px;
      margin: 0 auto;
    }
    #close {
      display: block;
      position: absolute;
      left: 0;
      top: 0;
      color: #FFF9C4;
      text-decoration: none;
      padding: 4px 8px;
    }
    #logo {
      float: left;
      margin: 0 5px;
    }
    #install {
      float: right;
      background: #FFF9C4;
      color: #E53935;
      text-decoration: none;
      font-weight: bold;
      padding: 4px 10px;
      border-radius: 4px;
    }
    #title {
      font-weight: bold;
      display: block;
      color: #FFF9C4;
      padding: 5px 0;
      margin-left: 82px;
    }
    #description {
      font-size: 80%;
      margin-left: 82px;
    }
  </style>
</head>
<body>
  <div id="container">
    <div id="about">
      <div class="clearfix">
        <a id="close" href="$url">&times;</a>
        <a id="install" href="http://app.appsflyer.com/com.hotify.me?pid=HFYLI" target="_blank">Install</a>
        <img id="logo" src="$pluginurl/img/ic_launcher.png" />
        <div id="title">Hotify</div>
        <div id="description">An Intelligent Mobile App that keeps you updated with Latest & trending News from various sources in a single place</div>
      </div>
    </div>
    <iframe id="frame" frameborder="0" noresize="noresize" src="$url"></iframe>
  </div>
</body>
</html>
PAGE;

  // Don't forget to die, to interrupt the flow of normal events (ie redirecting to long URL)
  die();
}
